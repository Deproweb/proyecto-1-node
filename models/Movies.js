//Se requiere mongoose
const mongoose = require('mongoose');

//Se crea una variable Schema, con el método de mongoose
const Schema = mongoose.Schema;

//Se crea un Schema, a través de un new Schema
const movieSchema = new Schema (
    {
        title : {type: String, required:true},
        director: {type: String},
        year: {type: Number},
        genre: {type: String}
    },
    {
        timestamps: true
    }
);

//Se crea el modelo, lo primero será el nombre de la colección, también se crea a partir del esquema.
const MovieModel = mongoose.model('Movie', movieSchema);


//Se exporta el modelo
module.exports = MovieModel;